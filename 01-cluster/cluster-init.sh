
# On Windows : Start Minikube cluster 
# Because the default service cluster IP is known to be available at 10.0.0.1, users can pull images from these registries
minikube start --vm-driver=hyperv --memory=6144 --cpus=2  --disk-size=50g  --kubernetes-version='v1.23.4'   --insecure-registry='10.0.0.0/24'

minikube addons enable ingress

minikube addons enable registry

minikube addons enable registry-aliases # The registry-aliases addon should allow you to build local 
                                        # container images with names like: example.com/foo/bar


# Now, let’s configure these registry-aliases in minikube by running the following:
kubectl apply -f https://raw.githubusercontent.com/kameshsampath/minikube-helpers/master/registry/registry-aliases-config.yaml

# Next, we should patch the core dns configuration of minikube and we can use the following set of commands:
kubectl apply -f https://raw.githubusercontent.com/kameshsampath/minikube-helpers/master/registry/node-etc-hosts-update.yaml
kubectl apply -f https://raw.githubusercontent.com/kameshsampath/minikube-helpers/master/registry/registry-aliases-sa.yaml
kubectl apply -f https://raw.githubusercontent.com/kameshsampath/minikube-helpers/master/registry/registry-aliases-sa-crb.yaml
kubectl apply -f patch-coredns-job.yaml

#Install Tekton and dashboard and triggers

kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml

kubectl apply --filename https://github.com/tektoncd/dashboard/releases/latest/download/tekton-dashboard-release.yaml

kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml
kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/interceptors.yaml

# Access to Tekton dashbord
kubectl --namespace tekton-pipelines port-forward svc/tekton-dashboard 9097:9097

# Install Argocd
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

# get the Argocd default password
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

#  Update argocd password
argocd account update-password

# Access to ArgoCD dashbord
kubectl port-forward svc/argocd-server -n argocd 8081:443



